const path = require('path');
//const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
	entry: './src/main.js',
	output: {
		path: path.resolve(__dirname, './dist'),
		publicPath: '/dist/',
		filename: 'build.js'
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
				test: /\.css$/,
				use: [ 'vue-style-loader', 'css-loader' ]
			},
			{
				test: /\.s[a|c]ss$/,
				use: [
					'style-loader', // creates style nodes from JS strings
					'css-loader', // translates CSS into CommonJS
					{
						loader: 'sass-loader',
						options: {indentedSyntax: true}
					}
				],
			},
			{
				test: /.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
			}
		],
	},
	plugins: [
//		new UglifyJSPlugin({ sourceMap: true }),
		new VueLoaderPlugin()
	],
	performance: {
		hints: false
	},
	devtool: '#eval-source-map'
};

if (process.env.NODE_ENV === 'production') {
	webpackConfig.devtool = '#source-map';
	webpackConfig.plugins = (webpackConfig.plugins || []).concat([
		// Short-circuit all warning code.
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: '"production"'
			}
		}),
		// Minify with dead-code elimination.
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				warnings: false
			},
			parallel: true
		})
	]);
}