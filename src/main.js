import Vue from 'vue';
import MainApp from './MyApp.vue';
import store from './classes/Store.js';

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

// import VueLayers from 'vuelayers';
// import 'vuelayers/lib/style.css';
// Vue.use(VueLayers, {});


Vue.use(Buefy, {
	defaultIconPack: 'fa',
});


const app = new Vue({
	el: '#app',
	store: store,
	render: (h) => {
		return h(MainApp);
	}
});

console.log(app);
