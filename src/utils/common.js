import {fromLonLat, toLonLat} from 'ol/proj.js';


export const generateUniqueId = (length, alphabet) => {
	length = length || 8;
	alphabet = alphabet || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	let alphabet_length = alphabet.length,
		result = '';
	for (let i = 0; i < length; i++) {
		result += alphabet.charAt(Math.floor(Math.random() * alphabet_length));
	}
	return result;
};


// Recursive transform all coordinates to Longtitude-Lattitude format.
export const convertCoordsRecursive = (coords, l, converter) => {
	if (l > 0) {
		let coords_lonlat = [];
		for (let i in coords) {
			if (coords.hasOwnProperty(i)) {
				coords_lonlat[i] = convertCoordsRecursive(coords[i], l - 1, converter);
			}
		}
		return coords_lonlat
	} else {
		return converter(coords);
	}
};

export const from3857_to4326 = (coords, levels) => {
	levels = (levels === undefined) ? 2 : levels;
	return convertCoordsRecursive(coords, levels, toLonLat);
};

export const from4326_to3857 = (coords, levels) => {
	levels = (levels === undefined) ? 2 : levels;
	return convertCoordsRecursive(coords, levels, fromLonLat);
};

