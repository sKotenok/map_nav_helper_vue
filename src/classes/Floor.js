import BaseModel from "./BaseGeoObject";
import User from "./User";
import Building from "./Building";
import {Draw, Modify, Snap} from "ol/interaction";
import {from3857_to4326, from4326_to3857} from "../utils/common";
import Point from "ol/geom/Point";
import Feature from "ol/Feature";
import Polygon from "ol/geom/Polygon";
import {Vector as VectorSource} from 'ol/source.js';
import {Vector as VectorLayer,} from 'ol/layer.js';


export default class Floor extends BaseModel {
	static get entity() { return 'floors'; }

	static fields() {
		return {
			id: this.attr(null),
			building_id: this.attr(null),
			user_id: this.attr(null),
			organization_id: this.attr(null),
			name: this.string(''),
			position: this.number(0),
			geodata: this.attr({}),
			bitmaps: this.attr([]),

			building: this.belongsTo(Building, 'building_id', 'id'),
			user: this.belongsTo(User, 'user_id', 'id'),
		};
	}

	asJson() {
		return {
			id: this.id,
			building_id: this.building_id,
			user_id: this.user_id,
			organization_id: this.organization_id,
			position: this.position,
			name: this.name,
			geodata: this.geodata,
		};
	}

	get _mapFeature() { return buildingFeatures[this.id]; }
	set _mapFeature(f) { buildingFeatures[this.id] = f; }

	setBackground() {
		let imageLayer = new ol.layer.ImageLayer({
			source: new ol.source.ImageStatic({
				url: 'http://127.0.0.1:5000/static/BITMAPS/alan.-1.png',
				projection: 'EPSG:4326',
				imageExtent: [-44, -23, -42, -21]
			})
		});
	}

	createFeature() {
		this._mapFeature = new Feature({
			geometry: new Polygon(from4326_to3857(this.geodata)),
			labelPoint: this.coords ? new Point(from4326_to3857(this.coords)) : undefined,
			name: this.name ? this.name : undefined
		});
		if ('id' in this && this.id) {
			this._mapFeature.setId(this.id);
		}
		return this._mapFeature;
	}

	drawFeature({map, vectorSource, onDrawStart, onDrawEnd}) {
		if (!this._drawInteraction) {
			this._drawInteraction = new Draw({source: vectorSource, type: 'Polygon'});
			map.addInteraction(this._drawInteraction);
		}
		if (!this._snapInteraction) {
			this._snapInteraction = new Snap({source: vectorSource});
			map.addInteraction(this._snapInteraction);
		}

		this._drawInteraction.on('drawstart', (event) => {
			this._mapFeature = event.feature;
			this._mapFeature.setId(this.id);
			if (onDrawStart) {
				onDrawStart(this, event);
			}
			console.log('BUILDING.ON DRAWSTART', event, this.id);
		});
		this._drawInteraction.on('drawend', (event) => {
			this.geodata = from3857_to4326(this._mapFeature.getGeometry().getCoordinates());
			map.removeInteraction(this._drawInteraction);
			map.removeInteraction(this._snapInteraction);
			this.$store().dispatch('entities/buildings/update', this);
			if (onDrawEnd) {
				onDrawEnd(this, event);
			}
			console.log('BUILDING.ON DRAWEND', event, this.id);
		});
	}

	editFeature({map, selection, onModifyEnd}) {
		if (!selection) {
			console.log('editFeature. Selection is empty!');
		}
		console.log('Begin modifying building', this.id);

		if (this._mapFeature) {
			selection.clear();
			selection.push(this._mapFeature);
		} else {
			let feature = this.vectorSources.buildings.getFeatureById(e.id);
			this._mapFeature = selection[0];
		}
		let modify = new Modify({
			features: selection,
			deleteCondition: (event) => {
				return (shiftKeyOnly(event) && singleClick(event)) || doubleClick(event);
			}
		});
		modify.on('modifyend', (event) => {
			if (event.features && event.features.getArray().length) {
				this.geodata = from3857_to4326(this._mapFeature.getGeometry().getCoordinates());
			}
			if (onModifyEnd) {
				onModifyEnd(this, event);
			}
		});
	}

	removeFeature(map) {
	}
}


