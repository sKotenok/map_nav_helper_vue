import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';
import VuexORM from '@vuex-orm/core';

import Building from './Building';
import Floor from './Floor';
import User from './User';
import {generateUniqueId} from "../utils/common";


const USERNAME = 'admin',
	PASSWORD = '12345';


Vue.use(Vuex);

const AUTH_TOKEN = '';

const geoApi = axios.create({
	baseURL: 'http://127.0.0.1:5000'
});
geoApi.defaults.headers.common['Authorization'] = AUTH_TOKEN;


const api = axios.create({
	baseURL: 'http://127.0.0.1:5000/api/v2',
	timeout: 3000,
	headers: {
	},
});

api.interceptors.request.use(
	function (config) {
		const token = store.state.auth.accessToken;
		if (token) {
			console.log('Set accessToken for request', config);
			config.headers.Authorization = `Bearer ${token}`;
		} else {
			console.warn('accessToken is not defined', config);
		}
		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);


const api_login = async () => {
	return await api.get(
		'/auth/login',
		{ params: {username: USERNAME, password: PASSWORD }}
	).then((response) => {
		console.log(response);
		return response;
	});
};



export const database = new VuexORM.Database();


const usersModule = {};
const buildingsModule = {};
const floorsModule = {};

database.register(User, usersModule);
database.register(Building, buildingsModule);
database.register(Floor, floorsModule);




const auth = {
	state: {
		accessToken: null,
		refreshToken: null,
		currentUserId: null,
	},
	getters: {},
	mutations: {
		SET_AUTH_TOKEN(state, accessToken) { state.accessToken = accessToken; },
		SET_REFRESH_TOKEN(state, refreshToken) { state.refreshToken = refreshToken; },
		SET_CURRENT_USER_ID(state, userId) { state.currentUserId = userId; },
	},
	actions: {
		async ensureLogin(context) {
			let resp = await api_login();

			if (resp && resp.status === 200 && resp.data) {
				const data = resp.data;
				context.commit('SET_AUTH_TOKEN', data.access_token);
				context.commit('SET_REFRESH_TOKEN', data.refresh_token);
				context.commit('SET_CURRENT_USER_ID', data.user_id);

				console.log('Authorization was succeed');
			}
		}
	},
};


const geoModule = {
	state: {
		countries: {},
		cities: {},

		buildings: {},
		currentBuildingId: null,
		buildingEntryPoints: {}, // {b_fid: Set(ep_fid, ep2_fid), ...}
		entryPoints: {}, // {ep_fid: [coords], ...}
		buildingCenters: {}, // {b_fid: c_fid, b2_fid: c_fid, ...}
		centerPoints: {}, // {cp_fid: [coords], ...}
		buildingFloors: {}, // {b_id: [f_id, f_id2,], ...}
		changedBuildings: {},

		floors: {},
		currentFloorId: null,
		floorBitmaps: {},
		bitmaps: {},
		floorGeoObjects: {},
		currentBitmapId: null,

		geoObjects: {},
		currentGeoObjectId: null,
	},
	getters: {
		buildingsArray(state, getters, rootState) {
			return state.buildings ? Object.values(state.buildings) : [];
		},
		currentBuilding(state, getters, rootState) {
			let curId = state.currentBuildingId;
			return (curId && curId in state.buildings)? state.buildings[curId]: null;
		},
		currentBuildingId(state, getters, rootState) { return state.currentBuildingId; },
		buildingEntryPoints(state, getters, rootState) { return (buildingId) => {
			if (buildingId in state.buildingEntryPoints) {
				return state.buildingEntryPoints[buildingId];
			} else {
				console.warn('getters.buildingEntryPoints: there is no entry points for building', buildingId);
				return [];
			}
		}},

		floorsInCurrentBuilding(state, getters, rootState) {
			let curBuildingId = state.currentBuildingId;
			let floorIds = state.buildingFloors[curBuildingId] || [];

			return Object.keys(state.floors)
				.filter(key => floorIds.includes(key))
				.reduce((obj, key) => {
					obj[key] = state.floors[key];
					return obj;
				}, {});
		},

		currentFloor(state, getters, rootState) {
			let curId = state.currentFloorId;
			return (curId && curId in state.floors) ? state.floors[curId] : null;
		},
		currentFloorId(state, getters, rootState) { return state.currentFloorId; },
		floorsArray(state, getters, rootState) {
			return state.floors ? Object.values(state.floors) : [];
		},

		currentBitmap(state, getters, rootState) {
			let curId = state.currentBitmapId;
			return (curId && curId in state.bitmaps) ? state.bitmaps[curId]: null;
		},


		currentGeoObject(state, getters, rootState) {
			let curId = state.currentGeoObjectId;
			return (curId && curId in state.geoObjects) ? state.geoObjects[curId] : null;
		},
	},
	mutations: {
		SET_BUILDING(state, building) {
			if ('id' in building) {
			} else {
				console.warn('SET_BUILDING: YOU ARE TRYING TO SET BUILDING WITHOUT ID:', building);
			}
			if (building.id in state.buildings) {
//				building = Object.assign({}, state.buildings[building.id], building);
				state.changedBuildings[building.id] = true;
			}
			console.log('SET_BUILDING', building);

			Vue.set(state.buildings, building.id, building);
		},
		SET_BUILDING_CENTER_POINT(state, center) {
			// center: {id: 123, coords: [coords]}
			if (!(center.buildingId in state.buildings)) {
				console.warn('SET_BUILDING_CENTER_POINT: There is no building with id', center);
				return;
			}
			Vue.set(state.centerPoints, center.id, center.coords);
			Vue.set(state.buildingCenters, center.buildingId, center.id);
		},
		SET_BUILDING_ENTRY_POINTS(state, entryObj) {
			let buildingId = entryObj.buildingId,
				entries = entryObj.entries;
			// entries: {id: [coords], id:[coords]}
			if (!(building_id in state.buildings)) {
				console.warn('SET_BUILDING_ENTRY_POINT: There is no building with id', building_id, entries);
				return;
			}
			if (!(building_id in state.buildingEntryPoints)) {
				Vue.set(state.buildingEntryPoints, building_id, new Set());
			}
			for (let entry of entries) {
				Vue.set(state.entryPoints, entry.id, entry.coords);
				state.buildingEntryPoints[building_id].add(entry.id); // Not a reactive chante, can't fix it for now!
			}
		},
		SET_BUILDINGS(state, buildings) {
			// It will be a difficult tu use code here.
			state.buildings = buildings;
		},
		DEL_BUILDING(state, building) {
			if ('id' in building) { Vue.delete(state.buildings, building.id); }
		},
		SET_CURRENT_BUILDING_ID(state, curId) {
			console.log('SET_CURRENT_BUILDING_ID', curId);
			state.currentBuildingId = curId;
		},

		SET_FLOOR(state, floor) {
			if (floor.id in state.floors) {
				floor = Object.assign({}, state.floors[floor.id], floor);
			}
			Vue.set(state.floors, floor.id, floor);
		},
		SET_CURRENT_FLOOR_ID(state, curId) {
			state.currentFloorId = curId;
		},
		DEL_FLOOR(state, floor) {
			if ('id' in floor) { Vue.delete(state.floors, floor.id); }
		},

		SET_CURRENT_BITMAP_ID(state, curId) {
			state.currentBitmapId = curId;
		},
		SET_BITMAP(state, bitmap) {
			if (!bitmap.id) { console.error('store.SET_BITMAP: THERE IS NO ID IN BITMAP!', bitmap); return; }
			if (bitmap.id in state.bitmaps) {
				bitmap = Object.assign({}, state.bitmaps[bitmap.id], bitmap);
			}
			Vue.set(state.bitmaps, bitmap.id, bitmap);

			console.log('changed bitmap:', bitmap);
		},

		SET_GEO_OBJECT(state, geoObject) {
			if (geoObject.id in state.geoObjects) {
				geoObject = Object.assign({}, state.geoObjects[geoObject.id], geoObject);
			}
			Vue.set(state.geoObjects, geoObject.id, geoObject);
		},
		SET_CUR_GEO_OBJECT_PROPERTIES(state, geoObjectProps) {
			if ('id' in geoObjectProps && geoObjectProps.id in state.geoObjects) {
				let geoObject = state.geoObjects[geoObjectProps.id];
				geoObject.properties = Object.assign({}, geoObject.properties, geoObjectProps);
				delete geoObject.properties.id;
				Vue.set(state.geoObjects, geoObject.id, geoObject);
			} else {
				console.warn('SET_CUR_GEO_OBJECT_PROPERTIES: There is no geoObject with this properties', geoObjectProps);
			}
		}


	},
	actions: {
		async loadBuildings(context, coords) {
			context.dispatch('ensureLogin').then(async () => {
				let url = '/buildings';
				if (coords) { url += '?lat=' + coords[0] + '&lng=' + coords[1]; }
				const resp = await api.get(url);
				if (resp.data && 'results' in resp.data && resp.data.results) {
					context.commit('SET_BUILDINGS', resp.data.results);
				} else {
					console.log('Error while getting buildings', resp);
				}
			});
		},

		async saveBuildings(context, buildingIds) {
			let bIdsToSave = {}, saveFromState = false;
			if (buildingIds) {
				for(let id in buildingIds) { if (buildingIds.hasOwnProperty(id)) {
					bIdsToSave[id] = true;
				}}
			} else {
				bIdsToSave = context.state.changedBuildings;
				saveFromState = true;
			}
			if (bIdsToSave) {
				let promises = [];
				for (let id in bIdsToSave) { if (bIdsToSave.hasOwnProperty(id)) {
					let b = context.state.buildings[id];
					if (b.isNew) {
						promises.push(api.post('/buildings', b.asJsonObject).then((resp) => {
							return true;
						}));
					} else {
						promises.push(api.put('/building/' + b.id, b.asJsonObject).then((resp) => {
							return true;
						}));
					}
				}}
				promises.then((resp) => {
					console.log('All buildings were saved!', resp);
					// Set ids for all buildings!
				});
			}
		},


		setBuilding(context, building) {
			context.commit('SET_BUILDING', building);
		},
		setBuildingsByFeatures(context, buildingFeatures) {
			if (buildingFeatures && buildingFeatures.length) {
				const len = buildingFeatures.length;
				for (let i = 0; i < len; i++) {
					let b = buildingFeatures[i];
					let id = 'tmp::' + b.id;
					console.log('setBuildingsByFeatures.BuildingFeature:', b);
					context.commit('SET_BUILDING', {
						'id': id,
						'geodata': b,
					});
				}
			} else {
				console.warn('setBuildingsByFeatures: buildingFeatures is empty!');
			}
		},
		setBuildingByFeature(context, building) {
			if (building && 'id' in building && 'geodata' in building) {
				context.commit('SET_BUILDING', building);
			}
		},
		delBuilding(context, building) {
			context.commit('DEL_BUILDING', building)
		},

		async changeBitmapImage(context, bitmap) {
			const form = new FormData();
			form.append('file', bitmap.file);
			const resp = await api.post(
				'/bitmap/upload-image',
				form, {
				headers: { 'Content-Type': 'multipart/form-data' }
			});
			if (resp && resp.status === 200 && resp.data) {

				console.log('Response data:', resp.data);

				for (let key of ['url', 'height', 'width']) {
					if (key in resp.data) {
						bitmap[key] = resp.data[key];
					}
				}
//				bitmap.url = resp.data.url;
//				delete bitmap.file;
				if (!bitmap.id) {
					bitmap.id = generateUniqueId(20);
				}
				context.commit('SET_BITMAP', bitmap);
			}
		}
	},
};



const store = new Vuex.Store({
	modules: {
		auth: auth,
		geo: geoModule,
	},
	plugins: [VuexORM.install(database)]
});


const BUILDINGS_DATA = [{
	"id": 1,
	'feature_id': 'SKNBw2NJTDrVkuvcbApB',
	"active": 1,
	"city_id": 2, // Набережные челны
	"organization_id": 2, // Рынок алан - организация, владеющая зданием
	"address": "Ул. Пушкина, дом колотушкина",
	"name": "Рынок Алан",
	"desc": "Носки, трусы и прочий ширпотреб", // Короткое описание.
	"geodata": [[
			[52.410940825939164, 55.76568744014449],
			[52.41220146417617, 55.764948045986415],
			[52.41318315267562, 55.76547015025497],
			[52.413827050477266, 55.76593519063735],
			[52.413302594795816, 55.76624306110597],
			[52.412410005927086, 55.76573006805367],
			[52.41173677146435, 55.76612465692946],
			[52.410940825939164, 55.76568744014449]
	]],

	"entries": [
		[52.410, 55.766],
		[52.413250, 55.766239],
		[52.413250, 55.766239]
	],
	"coords": [52.41224, 55.765427],
	"time_updated": "2018-05-31 05:34:20", // Время последнего изменения здания.
},{
	"id": 2,
	'feature_id': 'SKNBw2NJTDrVLuNcBbbPA',
	"city_id": 2, // Набережные челны
	"active": 1,
	"name": "Ипподром - Стадион",
	"address": "ул. Чулман, корпус",
	"organization_id": 2, // Рынок алан - организация, владеющая зданием
	"desc": "Стадион для лошадиных бегов", // Короткое описание.
	"geodata": [[
			[52.39961922168732, 55.7622000929733],
			[52.40490317344665, 55.76506423742558],
			[52.40524247288704, 55.76519174581421],
			[52.40555360913276, 55.76526870333811],
			[52.40599885582924, 55.76530190065449],
			[52.406379729509354, 55.765291337875055],
			[52.40677133202552, 55.76521588936748],
			[52.40720316767692, 55.76508913554608],
			[52.40750625729561, 55.76488844115275],
			[52.40768060088158, 55.764707362618054],
			[52.407795935869224, 55.76451421125418],
			[52.407828122377396, 55.76428635177436],
			[52.4077744781971, 55.76405849096295],
			[52.407504916191094, 55.76370311597995],
			[52.40204393863678, 55.760733231528775],
			[52.39961922168732, 55.7622000929733]
		]],
	"entries": [],
	"time_updated": "2018-05-31 05:34:20", // Время последнего изменения здания.
}];
const FLOORS_DATA = [{
	'id': 1,
	'building_id': 1,
	'position': 1,
	'name': '1й Этаж',
	'active': 1,
	'geodata': [],
	'bitmaps': [],
}];

for (let b_data of BUILDINGS_DATA) {
	Building.fromJSON(b_data);
}
for (let f_data of FLOORS_DATA) {
	Floor.fromJSON(f_data);
}



export default store;
