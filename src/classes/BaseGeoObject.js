import Feature from 'ol/Feature';
import Polygon from 'ol/geom/Polygon';
import Point from 'ol/geom/Point';
import LineString from 'ol/geom/LineString';
import {Draw, Modify, Snap, Select, Translate} from 'ol/interaction.js';
import {from3857_to4326, from4326_to3857, generateUniqueId} from '../utils/common';
import VuexORM from '@vuex-orm/core';




export default class BaseModel extends VuexORM.Model {

	static fromJSON(jsonData) {
		const store = this.store(),
			entity = this.entity;
		let result;
		let geoObject = store.getters['entities/'+entity+'/find'](jsonData.id);
		if (geoObject) {
			result =  store.dispatch('entities/'+entity+'/update', {
				where: geoObject.id,
				data: jsonData,
			});
		} else {
			result = store.dispatch('entities/'+entity+'/insert', { data: jsonData });
		}
		result._mapFeature = new Feature({

		});
		return result;
	}
}







const DEFAULT_TYPE = '',
	DEFAULT_GEOTYPE = 'point';


class GeoObject {
	constructor(params) {
		this.geoType = params.geoType || DEFAULT_GEOTYPE;
		this.geodata = params.geodata || {};

		if ('id' in params && params.id) {
			this.id = params.id;
			this._isNew = false;
		} else {
			this.id = generateUniqueId(20);
			this._isNew = true;
		}
		this._needSave = params.needSave || false;

		this._mapFeature = params.mapFeature || null;
		this._drawInteraction = params.drawInteraction || null;
		this._snapInteraction = params.snapInteraction || null;
		this._isSelected = params.isSelected || false;
	}

	static createFromJsonObject(jsonGeoObject) {
		let json = {
			id: 123,
			geodata: {},
			type_id: 1234,
			type: 'point',
			floor_id: 124,
			building_id: 1235,
			user_id: 111,
			organization_id: 345,
			time_updated: '',
			properties: {},
		};


		return new GeoObject();
	}

	get isNew() {
		return this._isNew;
	}

	set isNew(isNew) {
		this._isNew = isNew;
	}

	showOnMap(mapSource) {
	}

	hideOnMap(mapSource) {
	}

	drawFeature(map, mapSource, onDrawStart, onDrawEnd, drawInteraction, snapInteraction) {
		let result = {};
		if (!drawInteraction) {
			drawInteraction = new Draw({
				source: mapSource,
				type: this.geoType,
			});
			map.addInteraction(drawInteraction);
		}
		if (!snapInteraction) {
			snapInteraction = new Snap({source: mapSource});
			map.addInteraction(snapInteraction);
		}
		this._drawInteraction = drawInteraction;
		this._snapInteraction = snapInteraction;
		drawInteraction.on('drawstart', (e) => {
			if (!this.properties.id) {
				this.properties.id = generateUniqueId(20);
			}
			this._mapFeature.setId(this.properties.id);
			if (onDrawStart) {
				onDrawStart(this, e);
			}
			console.log('ON DRAWSTART', e, newId);
		});
		drawInteraction.on('drawend', (e) => {
			this.geodata = from3857_to4326(this._mapFeature.getGeometry().getCoordinates());
			map.removeInteraction(this.drawInteraction);
			if (onDrawEnd) {
				onDrawEnd(this, e);
			}
		});
	}

	createFeature(geoClass) {
		this._mapFeature = new Feature({
			geometry: new geoClass(from4326_to3857(this.geodata)),
			labelPoint: this.properties && this.properties.center ? new Point(from4326_to3857(b.center)) : undefined,
			name: this.properties && 'name' in this.properties ? this.properties.name : undefined
		});
		if ('id' in this.properties && this.properties.id) {
			this._mapFeature.setId(this.properties.id);
		}
	}

	editFeature(mapSource, geoClass) {

	}

	removeFeature(map) {

	}

	asJsonObject() {

	}


}


