import {Draw, Snap, Modify} from "ol/interaction";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import Polygon from 'ol/geom/Polygon';
import {Vector as VectorSource} from 'ol/source.js';
import {Vector as VectorLayer} from 'ol/layer.js';
import {Circle as CircleStyle, Fill, Stroke, Style} from "ol/style";

import BaseModel from './BaseGeoObject';
import User from './User';
import Floor from './Floor';
import {from3857_to4326, from4326_to3857, generateUniqueId} from "../utils/common";


const buildingFeatures = {},
	buildingPointsLayers = {},
	buildingSnapInteractions = {},
	buildingDrawInteractions = {};


export default class Building extends BaseModel {
	static get entity() { return 'buildings'; }

	static fields() {
		return {
			id: this.attr(null),
			city_id: this.attr(null),
			user_id: this.attr(null),
			user: this.belongsTo(User, 'user_id', 'id'),
			company_id: this.attr(null),
			name: this.string(''),
			address: this.string(''),
			short_description: this.string(''),
			geodata: this.attr({}),
			coords: this.attr([]),
			entries: this.attr([]),
			active: this.boolean(true),
			floors: this.hasMany(Floor, 'building_id'),
		};
	}

	// constructor(params) {
	// 	params = params || {};
	// 	super(params);
	//
	// 	this._geoType = 'polygon';
	//
	// 	this.active = params.active || true;
	// 	this.name = params.name || '';
	// 	this.shortDescription = params.shortDescription || '';
	//
	// 	this.coords = params.coords || null;
	// 	this.city_id = params.city_id || null;
	// 	this.entries = params.entries || [];
	// 	this.user_id = params.user_id || null;
	// 	this.organization_id = params.organization_id || null;
	//
	// 	this.feature = params._feature || null;
	// 	// new Feature({
	// 	// 	geometry: new Polygon(from4326_to3857(this.geodata)),
	// 	// 	labelPoint: this.coords ? new Point(from4326_to3857(this.coords)) : undefined,
	// 	// 	name: this.name,
	// 	// });
	// 	this._drawInteraction = params.drawInteraction || null;
	// 	this._snapInteraction = params.snapInteraction || null;
	// 	this._isSelected = params.isSelected || false;
	//
	// 	this.save();
	// }

	asJson() {
		return {
			id: this.id,
			city_id: this.city_id,
			active: this.active,
			name: this.name,
			short_description: this.short_description,
			user_id: this.user_id,
			organization_id: this.organization_id,
			entries: this.entries,
			geodata: this.geodata,
			coords: this.coords,
		};
	}

	get isNew() {return this._isNew;}
	set isNew(isNew) {this._isNew = isNew;}

	get _mapFeature() { return buildingFeatures[this.id]; }
	set _mapFeature(f) { buildingFeatures[this.id] = f; }

	get _innerLayer() { return buildingPointsLayers[this.id]; }
	set _innerLayer(l) { buildingPointsLayers[this.id] = l; }


	showOnMap(mapSource) {}

	hideOnMap(mapSource) {}

	createFeature() {
		this._mapFeature = new Feature({
			geometry: new Polygon(from4326_to3857(this.geodata)),
			labelPoint: this.coords ? new Point(from4326_to3857(this.coords)) : undefined,
			name: this.name ? this.name : undefined
		});
		if ('id' in this && this.id) {
			this._mapFeature.setId(this.id);
		}
		return this._mapFeature;
	}

	drawFeature({map, vectorSource, onDrawStart, onDrawEnd}) {
		if (!this._drawInteraction) {
			this._drawInteraction = new Draw({ source: vectorSource, type: 'Polygon' });
			map.addInteraction(this._drawInteraction);
		}
		if (!this._snapInteraction) {
			this._snapInteraction = new Snap({source: vectorSource});
			map.addInteraction(this._snapInteraction);
		}

		this._drawInteraction.on('drawstart', (event) => {
			this._mapFeature = event.feature;
			this._mapFeature.setId(this.id);
			if (onDrawStart) {
				onDrawStart(this, event);
			}
			console.log('BUILDING.ON DRAWSTART', event, this.id);
		});
		this._drawInteraction.on('drawend', (event) => {
			this.geodata = from3857_to4326(this._mapFeature.getGeometry().getCoordinates());
			map.removeInteraction(this._drawInteraction);
			map.removeInteraction(this._snapInteraction);
			this.$store().dispatch('entities/buildings/update', this);
			if (onDrawEnd) {
				onDrawEnd(this, event);
			}
			console.log('BUILDING.ON DRAWEND', event, this.id);
		});
	}

	editFeature({map, selection, onModifyEnd}) {
		if (!selection) {
			console.log('editFeature. Selection is empty!');
		}
		console.log('Begin modifying building', this.id);

		if (this._mapFeature) {
			selection.clear();
			selection.push(this._mapFeature);
		} else {
			let feature = this.vectorSources.buildings.getFeatureById(e.id);
			this._mapFeature = selection[0];
		}
		let modify = new Modify({
			features: selection,
			deleteCondition: (event) => {
				return (shiftKeyOnly(event) && singleClick(event)) || doubleClick(event);
			}
		});
		modify.on('modifyend', (event) => {
			if (event.features && event.features.getArray().length) {
				this.geodata = from3857_to4326(this._mapFeature.getGeometry().getCoordinates());
			}
			if (onModifyEnd) {
				onModifyEnd(this, event);
			}
		});
	}

	removeFeature(map) {
	}

	drawCenter(map) {
		if (this._drawInteraction) { map.removeInteraction(this._drawInteraction)}
		this._drawInteraction = new Draw({
			source: this._innerLayer.getSource(),
			type: 'Point',
		});
		map.addInteraction(this._drawInteraction);

		this._drawInteraction.on('drawend', (event) => {
			let feature = event.feature,
				source = this._innerLayer.getSource();

			let old_feature = source.getFeatureById( this.centerPointFeatureId() );
			if (old_feature) {
				source.removeFeature(old_feature);
			}

			feature.setId( this.centerPointFeatureId());
			let coords = feature.getGeometry().getCoordinates();
			console.log('ON CENTER POINT DRAWEND', event, feature.getId(), coords);

			this.coords = from3857_to4326(coords, 0);
			this.$store().dispatch('entities/buildings/update', this);

			map.removeInteraction(this._drawInteraction);
			this._drawInteraction = null;
		});
	}


	showPoints(map) {
		if (this._innerLayer) {
			this._innerLayer.setVisible(true);
		} else {
			let source = new VectorSource();

			if (this.coords) {
				console.log(from4326_to3857(this.coords, 0));
				let f = new Feature({
					geometry: new Point(from4326_to3857(this.coords, 0)),
				});
				f.setId(this.centerPointFeatureId());
				source.addFeature(f);
			}
			if (this.entries && this.entries.length > 0) {
				for (let i = 0; i < this.entries.length; i++) {
					let cp = this.entries[i];
					console.log(from4326_to3857(cp, 0));
					let f = new Feature({
						geometry: new Point(from4326_to3857(cp, 0)),
					});
					f.setId( this.entryPointFeatureId(i) );
					source.addFeature(f);
				}
			}

			this._innerLayer = new VectorLayer({
				source: source,
				visible: true,
				opacity: 1,
				style: new Style({
					fill: new Fill({
						color: 'rgba(255, 255, 255, 0.5)'
					}),
					stroke: new Stroke({
						color: '#4ac8ff',
						width: 2
					}),
					image: new CircleStyle({
						radius: 7,
						fill: new Fill({
							color: '#ff000c'
						})
					})
				})
			});
			this._innerLayer.setZIndex(10);

			console.log('Inner Layer:', this._innerLayer, this._innerLayer.ol_uid);
			console.log('Inner Layer.Source:', source, source.ol_uid);
			for (let f of this._innerLayer.getSource().getFeatures()) {
				console.log('SHOW_POINTS.Coordinates', f.getGeometry().getCoordinates());
			}


			map.addLayer(this._innerLayer);
		}
	}
	hidePoints(map) {
		if (this._innerLayer) {
			this._innerLayer.setVisible(false);
		}
	}

	centerPointFeatureId() { return 'c:' + this.id; }
	entryPointFeatureId(count) { return 'e:' + count + ':' + this.id; }

}


