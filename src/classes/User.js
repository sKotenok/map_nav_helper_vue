import BaseModel from './BaseGeoObject';

export default class User extends BaseModel {
	static get entity() {
		return 'users';
	}

	static fields() {
		return {
			id: this.attr(null),
			name: this.string(''),
		};
	}
}
