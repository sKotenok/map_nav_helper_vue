import Feature from 'ol/Feature';
import Polygon from 'ol/geom/Polygon';
import Point from 'ol/geom/Point';
import LineString from 'ol/geom/LineString';
import {Draw, Modify, Snap, Select, Translate} from 'ol/interaction.js';
import {from3857_to4326, from4326_to3857, generateUniqueId} from '../utils/common';


const   DEFAULT_TYPE       = '',
		DEFAULT_GEOTYPE    = 'point',
		TYPE_GEOTYPES = {
//			''
		};



export default class GeoObject {
	static createFromJsonObject(jsonData) {
		let json = {
			id: 123,
			geodata: {},
			type_id: 1234,
			type: 'point',
			floor_id: 124,
			building_id: 1235,
			user_id: 111,
			organization_id: 345,
			time_updated: '',
			properties: {},
		};

		return new GeoObject({
			id: jsonData.id || null,
			geoType: ''
		});
	}

	get _geoType() { return TYPE_GEOTYPES[this.type_id]; }

	get isNew() { return this._isNew; }
	set isNew(isNew) { this._isNew = isNew; }

	showOnMap(mapSource) {}
	hideOnMap(mapSource) {}

	drawFeature({map, vectorSource, onDrawStart, onDrawEnd}) {
		if (!this._drawInteraction) {
			this._drawInteraction = new Draw({source: vectorSource, type: 'Polygon'});
			map.addInteraction(this._drawInteraction);
		}
		if (!this._snapInteraction) {
			this._snapInteraction = new Snap({source: vectorSource});
			map.addInteraction(this._snapInteraction);
		}

		this._drawInteraction.on('drawstart', (event) => {
			this._mapFeature = event.feature;
			this._mapFeature.setId(this.id);
			if (onDrawStart) {
				onDrawStart(this, event);
			}
			console.log('GEO_OBJECT.ON DRAWSTART', event, this.id);
		});
		this._drawInteraction.on('drawend', (event) => {
			this.geodata = from3857_to4326(this._mapFeature.getGeometry().getCoordinates());
			map.removeInteraction(this._drawInteraction);
			map.removeInteraction(this._snapInteraction);
			this.$store().dispatch('entities/geo-objects/update', this);
			if (onDrawEnd) {
				onDrawEnd(this, event);
			}
			console.log('GEO_OBJECT.ON DRAWEND', event, this.id);
		});
	}

	createFeature(geoClass) {
		this._mapFeature = new Feature({
			geometry: new geoClass(from4326_to3857(this.geodata)),
			labelPoint: this.properties && this.properties.center ? new Point(from4326_to3857(b.center)) : undefined,
			name: this.properties && 'name' in this.properties ? this.properties.name : undefined
		});
		if ('id' in this.properties && this.properties.id) {
			this._mapFeature.setId(this.properties.id);
		}
	}

	editFeature(mapSource, geoClass) {

	}

	removeFeature(map) {

	}

	asJsonObject() {

	}


}


